#!/bin/bash

docker cp ./conf/manager.conf asterisk:/etc/asterisk
docker cp ./conf/http.conf asterisk:/etc/asterisk

docker exec -it asterisk sh -c "asterisk -rx 'module reload'"
